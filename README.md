# Welcome
 - This repo uses `yarn`
 - It's a React App, in Typescript, created with ViteJS 


# Weather API
https://openweathermap.org/api/one-call-3

API Key : 0e18be9b87d0e7a0dc83bcc1019ccf4d


```bash 
curl -X GET 'https://api.openweathermap.org/data/2.5/weather?appid=0e18be9b87d0e7a0dc83bcc1019ccf4d&q=chicago' -H 'Accept: application/json'  
```
